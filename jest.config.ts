// dependencies
// jest @types/jest ts-jest jest-environment-jsdom jest-fetch-mock
// @testing-library/dom @testing-library/jest-dom @testing-library/react @testing-library/user-event identity-obj-proxy

import type { Config } from 'jest';

const config: Config = {
	preset: 'ts-jest/presets/js-with-ts',
	clearMocks: true, // clear mocks before each test
	automock: false, // for jest-fetch-mock
	resetMocks: false, // for jest-fetch-mock
	testEnvironment: 'jsdom',
	moduleDirectories: ['node_modules', 'src'], // for import
	moduleNameMapper: {
		'\\.(css|less|scss|sass)$': 'identity-obj-proxy',
		'@root(.*)$': '<rootDir>/src/$1',
	},
	transform: {
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/jest/file-transformer.ts',
	},
	transformIgnorePatterns: ['node_modules'],
	setupFiles: ['<rootDir>/jest/setup-jest.ts'], // before each
};

export default config;
