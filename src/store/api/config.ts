import { fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { RootState } from '../types';

export const customBaseQuery = fetchBaseQuery({
	baseUrl: 'https://api.react-learning.ru',
	prepareHeaders: (headers, { getState }) => {
		const accessToken = (getState() as RootState).rootLayout.tokens.accessToken;

		if (accessToken) {
			headers.set('authorization', `Bearer ${accessToken}`);
		}

		return headers;
	},
});
