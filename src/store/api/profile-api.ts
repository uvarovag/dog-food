import { TUser } from '../../models/models';
import { createApi } from '@reduxjs/toolkit/dist/query/react';
import { customBaseQuery } from './config';
import {
	TAvatarFormValue,
	TProfileFormValue,
} from '../../pages/profile-edit-page/profile-edit-page';

export type BE_TUser = Omit<TUser, 'id'> & {
	_id: TUser['id'];
};

export function modifyUser(BE_User: BE_TUser) {
	const { _id, ...rest } = BE_User;
	return { id: _id, ...rest };
}

export const profileApi = createApi({
	reducerPath: 'profile',
	baseQuery: customBaseQuery,
	tagTypes: ['Profile'],
	endpoints: (builder) => ({
		editProfile: builder.mutation<TUser, TProfileFormValue>({
			query: (profileEditFormValues) => ({
				url: 'users/me',
				method: 'PATCH',
				body: profileEditFormValues,
			}),
			invalidatesTags: ['Profile'],
			transformResponse: modifyUser,
		}),
		editAvatar: builder.mutation<TUser, TAvatarFormValue>({
			query: (avatarEditFormValues) => ({
				url: 'users/me/avatar',
				method: 'PATCH',
				body: avatarEditFormValues,
			}),
			invalidatesTags: ['Profile'],
			transformResponse: modifyUser,
		}),
	}),
});

export const { useEditProfileMutation, useEditAvatarMutation } = profileApi;
