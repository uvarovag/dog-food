import storage from 'redux-persist/lib/storage';
import { authApi } from '../api/auth-api';
import { combineReducers } from '@reduxjs/toolkit';
import { rootLayoutSlice } from './root-layout/root-layout-slice';
import { persistReducer } from 'redux-persist';
import { productsApi } from '../api/products-api';
import { cartSlice } from './cart/cart-slice';
import { profileApi } from '../api/profile-api';

const persistConfig = {
	key: 'root',
	storage,
	version: 1,
	blacklist: [
		authApi.reducerPath,
		productsApi.reducerPath,
		profileApi.reducerPath,
	],
};

export const rootReducer = combineReducers({
	[rootLayoutSlice.name]: rootLayoutSlice.reducer,
	[cartSlice.name]: cartSlice.reducer,
	[authApi.reducerPath]: authApi.reducer,
	[profileApi.reducerPath]: profileApi.reducer,
	[productsApi.reducerPath]: productsApi.reducer,
});

export const persistedReducer = persistReducer(persistConfig, rootReducer);
