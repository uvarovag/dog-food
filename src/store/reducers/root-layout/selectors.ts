import { RootState } from '../../types';
import { rootLayoutSlice } from './root-layout-slice';

export const selectTokens = (state: RootState) => {
	return state[rootLayoutSlice.name].tokens;
};

export const selectUser = (state: RootState) => {
	return state[rootLayoutSlice.name].user;
};

export const selectHeaderSearchQuery = (state: RootState) => {
	return state[rootLayoutSlice.name].headerSearchQuery;
};
