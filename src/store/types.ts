import store from './store';
// eslint-disable-next-line import/no-unresolved
import { FieldValues } from 'react-hook-form/dist/types/fields';
import { FormEvent } from 'react';
import { Control, DeepRequired, FieldErrorsImpl } from 'react-hook-form';

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export type TMutationProps<T extends FieldValues> = {
	onSubmit: (e: FormEvent<HTMLFormElement>) => void;
	control: Control<T>;
	errors: Partial<FieldErrorsImpl<DeepRequired<T>>>;
	isValid: boolean;
	isSubmitting: boolean;
	isSubmitted: boolean;
};
