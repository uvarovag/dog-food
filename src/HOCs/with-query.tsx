import { ComponentType, FC } from 'react';
import { Button, CircularProgress, Stack } from '@mui/material';
import { toast } from 'react-toastify';

type WithQueryType = {
	isLoading: boolean;
	isError: boolean;
	error?: string;
	refetch?: () => void;
};

export const withQuery = <P extends object>(
	WrappedComponent: ComponentType<P>
) => {
	const ReturnedComponent: FC<P & WithQueryType> = (props) => {
		const {
			isLoading,
			isError,
			error,
			refetch,
			...restPropsForWrappedComponent
		} = props;

		if (isLoading) {
			return (
				<Stack direction={'column'}>
					<CircularProgress sx={{ mx: 'auto', my: 5 }} />
				</Stack>
			);
		}

		if (isError) {
			toast.error(error);
			return (
				<Stack direction={'column'}>
					<Button onClick={refetch} sx={{ mx: 'auto', my: 5 }}>
						refetch
					</Button>
				</Stack>
			);
		}

		return <WrappedComponent {...(restPropsForWrappedComponent as P)} />;
	};

	ReturnedComponent.displayName = `withQuery${ReturnedComponent.displayName}`;
	return ReturnedComponent;
};
