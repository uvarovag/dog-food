import { ComponentType, FC } from 'react';
import { Box, Checkbox, SxProps } from '@mui/material';

export type TWithSelectProps<I> = {
	selected: boolean;
	onSelect: (item: I, newValue: boolean) => void;
	checkBoxSx?: SxProps;
};

export const withSelect = <P extends object>(
	WrappedComponent: ComponentType<P>
) => {
	const ReturnedComponent: FC<P & TWithSelectProps<P>> = (props) => {
		const {
			selected,
			onSelect,
			checkBoxSx = {},
			...restPropsForWrappedComponent
		} = props;

		const onChange = () => {
			onSelect(restPropsForWrappedComponent as P, selected);
		};

		return (
			<Box sx={{ position: 'relative' }}>
				<Checkbox
					checked={selected}
					onChange={onChange}
					sx={{
						...checkBoxSx,
						position: 'absolute',
						top: '-1px',
						left: '-1px',
					}}
				/>
				<WrappedComponent {...(restPropsForWrappedComponent as P)} />
			</Box>
		);
	};

	ReturnedComponent.displayName = `withSelectItem${ReturnedComponent.displayName}`;
	return ReturnedComponent;
};
