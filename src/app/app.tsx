import {
	createBrowserRouter,
	createRoutesFromElements,
	Route,
	RouteObject,
	RouterProvider,
} from 'react-router-dom';
import RootLayout from '../layouts/root-layout/root-layout';
import MainPage from '../pages/main-page/main-page';
import ProductsPage from '../pages/products-page/products-page';
import ProductPage from '../pages/product-page/product-page';
import ProfilePage from '../pages/profile-page/profile-page';
import ProfileEditPage from '../pages/profile-edit-page/profile-edit-page';
import FavouritesPage from '../pages/favorites-page/favorites-page';
import NotFoundPage from '../pages/not-found-page/not-found-page';
import SignUpPage from '../pages/sign-up-page/sign-up-page';
import SignInPage from '../pages/sign-in-page/sign-in-page';
import CartPage from '../pages/cart-page/cart-page';
import ProductCreatePage from '../pages/product-create-page/product-create-page';
import ProductEditPage from '../pages/product-edit-page/product-edit-page';

export const routerConfig: RouteObject[] = createRoutesFromElements(
	<Route
		path='/'
		id='root'
		element={<RootLayout />}
		errorElement={<NotFoundPage />}>
		<Route index element={<MainPage />} />
		<Route path='products' element={<ProductsPage />} />
		<Route path='product/:productId' element={<ProductPage />} />
		<Route path='product-create' element={<ProductCreatePage />} />
		<Route path='product-edit/:productId' element={<ProductEditPage />} />
		<Route path='favorites' element={<FavouritesPage />} />
		<Route path='cart' element={<CartPage />} />
		<Route path='profile' element={<ProfilePage />} />
		<Route path='profile-edit' element={<ProfileEditPage />} />
		<Route path='signup' element={<SignUpPage />} />
		<Route path='signin' element={<SignInPage />} />
	</Route>
);

const App = () => {
	const router = createBrowserRouter(routerConfig);

	return <RouterProvider router={router} />;
};

export default App;
