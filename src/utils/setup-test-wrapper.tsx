import { createMemoryRouter, RouterProvider } from 'react-router-dom';
import { render } from '@testing-library/react';
import { ThemeProvider } from '@mui/material';
import { theme } from '../app/theme';
import { Provider } from 'react-redux';
import store from '../store/store';
import { ToastContainer } from 'react-toastify';
import { routerConfig } from '../app/app';
import './../i18n/i18n';

type TWrapperForTestParams = {
	initialRouterPath: string;
};

export const setupTestWrapper = ({
	initialRouterPath,
}: TWrapperForTestParams) => {
	const router = createMemoryRouter(routerConfig, {
		initialEntries: [initialRouterPath],
	});
	render(
		<ThemeProvider theme={theme}>
			<Provider store={store}>
				<RouterProvider router={router} />
				<ToastContainer theme={'colored'} position={'top-right'} />
			</Provider>
		</ThemeProvider>
	);

	return router;
};
