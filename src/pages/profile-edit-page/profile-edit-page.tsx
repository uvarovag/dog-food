import { FC } from 'react';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { Link, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
	useEditAvatarMutation,
	useEditProfileMutation,
} from '../../store/api/profile-api';
import * as yup from 'yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import UserEdit, {
	TUserEditFormValue,
} from '../../components/user-edit/user-edit';
import { yupResolver } from '@hookform/resolvers/yup';
import { TUser } from '../../models/models';
import { setUser } from '../../store/reducers/root-layout/root-layout-slice';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import { Button, Stack } from '@mui/material';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import Spacer from '../../components/spacer/spacer';
import { withProtection } from '../../HOCs/with-protection';

export type TProfileFormValue = Pick<TUserEditFormValue, 'name' | 'about'>;
export type TAvatarFormValue = Pick<TUserEditFormValue, 'avatar'>;

const ProfileEditPage: FC = () => {
	const NAVIGATE_PATH_SUCCESS = '/profile';
	const NAVIGATE_PATH_CANCEL = '/profile';
	const user = useAppSelector(selectUser);
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [editProfileMutation] = useEditProfileMutation();
	const [editAvatarMutation] = useEditAvatarMutation();

	const formSchema = yup.object({
		name: yup.string().required().min(4).max(24),
		about: yup.string().required().min(4).max(100),
		avatar: yup.string().required().min(4).max(200),
	});

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TUserEditFormValue>({
		defaultValues: {
			name: user?.name,
			about: user?.about,
			avatar: user?.avatar,
		},
		resolver: yupResolver(formSchema),
	});

	const submitHandler: SubmitHandler<TUserEditFormValue> = async (values) => {
		try {
			let response: TUser | undefined;
			if (user && (user.name !== values.name || user.about !== values.about)) {
				response = await editProfileMutation({
					name: values.name,
					about: values.about,
				}).unwrap();
			}
			if (user && user.avatar !== values.avatar) {
				response = await editAvatarMutation({
					avatar: values.avatar,
				}).unwrap();
			}
			if (response) {
				dispatch(setUser(response));
				toast.success(t('message.changesSaved'));
				navigate(NAVIGATE_PATH_SUCCESS, {
					state: { from: location.pathname },
				});
			} else {
				toast.warning(t('message.nothingChanged'));
			}
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button component={Link} to={NAVIGATE_PATH_CANCEL} sx={{ mb: 2 }}>
					{t('button.cancel')}
				</Button>
			</Stack>
			<UserEdit
				user={user}
				onSubmit={handleSubmit(submitHandler)}
				control={control}
				errors={errors}
				isValid={isValid}
				isSubmitting={isSubmitting}
				isSubmitted={isSubmitted}
			/>
		</>
	);
};

export default withProtection(ProfileEditPage);
