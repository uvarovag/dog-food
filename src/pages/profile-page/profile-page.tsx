import { FC } from 'react';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import User from '../../components/user/user';
import { useAppSelector } from '../../store/hooks';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { withProtection } from '../../HOCs/with-protection';
import { Button, Stack } from '@mui/material';
import Spacer from '../../components/spacer/spacer';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const ProfilePage: FC = () => {
	const NAVIGATE_PATH_EDIT = '/profile-edit';
	const user = useAppSelector(selectUser);
	const { t } = useTranslation();
	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button component={Link} to={NAVIGATE_PATH_EDIT} sx={{ mb: 2 }}>
					{t('button.edit')}
				</Button>
			</Stack>
			<User user={user} />
		</>
	);
};

export default withProtection(ProfilePage);
