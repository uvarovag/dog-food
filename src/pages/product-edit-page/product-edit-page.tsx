import { FC, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
	useEditProductMutation,
	useFetchProductQuery,
} from '../../store/api/products-api';
import { SubmitHandler, useForm } from 'react-hook-form';
import { TProductCreateFormValue } from '../../components/product-create/product-create';
import { yupResolver } from '@hookform/resolvers/yup';
import { productFormSchema } from '../product-create-page/product-create-page';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import { Button, Stack } from '@mui/material';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import Spacer from '../../components/spacer/spacer';
import ProductEdit from '../../components/product-edit/product-edit';
import { withProtection } from '../../HOCs/with-protection';

const ProductEditPage: FC = () => {
	const navigatePathProductCard = (productId: string): string =>
		`/product/${productId}`;
	const navigate = useNavigate();
	const { t } = useTranslation();
	const { productId } = useParams();
	const [editProductMutation] = useEditProductMutation();

	const { data, isLoading, isError, error, refetch } = useFetchProductQuery(
		productId || '',
		{ skip: !productId }
	);

	const {
		control,
		handleSubmit,
		setValue,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TProductCreateFormValue>({
		defaultValues: {
			price: 0,
			discount: 0,
			stock: 0,
			description: '',
			name: '',
			pictures: '',
		},
		resolver: yupResolver(productFormSchema),
	});

	useEffect(() => {
		if (data) {
			setValue('price', data.price);
			setValue('discount', data.discount);
			setValue('stock', data.stock);
			setValue('description', data.description);
			setValue('name', data.name);
			setValue('pictures', data.pictures);
		}
	}, [data, setValue]);

	const submitHandler: SubmitHandler<TProductCreateFormValue> = async (
		values
	) => {
		try {
			const response = await editProductMutation({
				productId: productId || '',
				...values,
			}).unwrap();
			toast.success(t('message.changesSaved'));
			navigate(navigatePathProductCard(response.id), {
				state: { from: location.pathname },
			});
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button
					component={Link}
					to={navigatePathProductCard(productId || '')}
					sx={{ mb: 2 }}>
					{t('button.cancel')}
				</Button>
			</Stack>
			<ProductEdit
				onSubmit={handleSubmit(submitHandler)}
				control={control}
				errors={errors}
				isValid={isValid}
				isSubmitting={isSubmitting}
				isSubmitted={isSubmitted}
				isLoading={isLoading}
				isError={isError}
				error={getMessageFromError(error, t('message.unexpectedError'))}
				refetch={refetch}
			/>
		</>
	);
};

export default withProtection(ProductEditPage);
