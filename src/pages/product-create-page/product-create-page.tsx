import { FC } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useCreateProductMutation } from '../../store/api/products-api';
import * as yup from 'yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import ProductCreate, {
	TProductCreateFormValue,
} from '../../components/product-create/product-create';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import { Button, Stack } from '@mui/material';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import Spacer from '../../components/spacer/spacer';
import { withProtection } from '../../HOCs/with-protection';

export const productFormSchema = yup.object({
	price: yup.number().integer().positive().required(),
	discount: yup.number().integer().positive().required(),
	stock: yup.number().integer().positive().required(),
	description: yup.string().required().min(4).max(200),
	name: yup.string().required().min(4).max(200),
	pictures: yup.string().required().min(4).max(200),
});

const ProductCreatePage: FC = () => {
	const NAVIGATE_PATH_CANCEL = '/products';
	const navigateSuccessPath = (productId: string): string =>
		`/product/${productId}`;
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [createProductMutation] = useCreateProductMutation();

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TProductCreateFormValue>({
		defaultValues: {
			price: 0,
			discount: 0,
			stock: 0,
			description: '',
			name: '',
			pictures: '',
		},
		resolver: yupResolver(productFormSchema),
	});

	const submitHandler: SubmitHandler<TProductCreateFormValue> = async (
		values
	) => {
		try {
			const response = await createProductMutation(values).unwrap();
			toast.success(t('message.changesSaved'));
			navigate(navigateSuccessPath(response.id), {
				state: { from: location.pathname },
			});
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button component={Link} to={NAVIGATE_PATH_CANCEL} sx={{ mb: 2 }}>
					{t('button.cancel')}
				</Button>
			</Stack>
			<ProductCreate
				onSubmit={handleSubmit(submitHandler)}
				control={control}
				errors={errors}
				isValid={isValid}
				isSubmitting={isSubmitting}
				isSubmitted={isSubmitted}
			/>
		</>
	);
};

export default withProtection(ProductCreatePage);
