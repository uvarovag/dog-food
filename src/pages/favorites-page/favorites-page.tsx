import { useTranslation } from 'react-i18next';
import { useAppSelector } from '../../store/hooks';
import {
	selectHeaderSearchQuery,
	selectUser,
} from '../../store/reducers/root-layout/selectors';
import { useFetchFavoriteProductsQuery } from '../../store/api/products-api';
import { useMemo } from 'react';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import ProductsGrid from '../../components/products-grid/products-grid';
import { getMessageFromError } from '../../utils/errorUtil';
import { withProtection } from '../../HOCs/with-protection';
import { useDebounce } from '../../hooks/useDebouce';

const FavoritesPage = () => {
	const { t } = useTranslation();
	const user = useAppSelector(selectUser);
	const query = useAppSelector(selectHeaderSearchQuery);
	const debounceQuery = useDebounce<string>(query);
	const { data, isLoading, isError, error, refetch } =
		useFetchFavoriteProductsQuery(debounceQuery || undefined);
	const products = useMemo(() => {
		if (!user || !data || !data.products) {
			return [];
		}
		return data.products.filter((product) => {
			return product.likes.includes(user.id);
		});
	}, [user, data]);
	return (
		<>
			<NavBackButton sx={{ mb: 2 }} />
			<ProductsGrid
				products={products}
				isLoading={isLoading}
				isError={isError}
				error={getMessageFromError(error, t('message.unexpectedError'))}
				refetch={refetch}
			/>
		</>
	);
};

export default withProtection(FavoritesPage);
