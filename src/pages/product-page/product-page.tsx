import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams, useNavigate } from 'react-router-dom';
import {
	useDeleteProductMutation,
	useFetchProductQuery,
} from '../../store/api/products-api';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import Product from '../../components/product/product';
import { getMessageFromError } from '../../utils/errorUtil';
import { withProtection } from '../../HOCs/with-protection';
import { Button, Stack } from '@mui/material';
import Spacer from '../../components/spacer/spacer';
import { toast } from 'react-toastify';
import { TEST_ID } from '../../constants/constants';

const ProductPage: FC = () => {
	const NAVIGATE_PATH_SUCCESS_DELETE = '/products';
	const { t } = useTranslation();
	const navigate = useNavigate();
	const { productId } = useParams();
	const navigatePathEdit = (): string => `/product-edit/${productId}`;
	const { data, isLoading, isError, error, refetch } = useFetchProductQuery(
		productId || '',
		{ skip: !productId }
	);
	const [deleteProductMutation] = useDeleteProductMutation();

	const deleteProduct = async () => {
		try {
			await deleteProductMutation(productId || '').unwrap();
			toast.success(t('message.changesSaved'));
			navigate(NAVIGATE_PATH_SUCCESS_DELETE, {
				state: { from: location.pathname },
			});
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button
					onClick={deleteProduct}
					sx={{ mb: 2 }}
					data-testid={TEST_ID.BUTTON.DELETE_PRODUCT_PRODUCTCARD_BUTTON}>
					{t('button.delete')}
				</Button>
				<Button component={Link} to={navigatePathEdit()} sx={{ mb: 2 }}>
					{t('button.edit')}
				</Button>
			</Stack>
			<Product
				product={data || null}
				isLoading={isLoading}
				isError={isError}
				error={getMessageFromError(error, t('message.unexpectedError'))}
				refetch={refetch}
			/>
		</>
	);
};

export default withProtection(ProductPage);
