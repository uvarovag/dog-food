import { InputBase, SxProps } from '@mui/material';
import { alpha, styled } from '@mui/material/styles';
import { FC, useEffect, useState } from 'react';
import { useAppDispatch } from '../../store/hooks';
import { useSearchParams } from 'react-router-dom';
import { setHeaderSearchQuery } from '../../store/reducers/root-layout/root-layout-slice';
import SearchIcon from '@mui/icons-material/Search';

const DEFAULT_PLACEHOLDER = 'Search...';

type TSearchProps = {
	placeholder?: string;
	sx?: SxProps;
};

const BaseSearch = styled('div')(({ theme }) => ({
	position: 'relative',
	borderRadius: theme.shape.borderRadius,
	backgroundColor: alpha(theme.palette.common.white, 0.15),
	'&:hover': {
		backgroundColor: alpha(theme.palette.common.white, 0.25),
	},
	marginLeft: 0,
	width: '100%',
	[theme.breakpoints.up('sm')]: {
		marginLeft: theme.spacing(1),
		width: 'auto',
	},
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
	padding: theme.spacing(0, 2),
	height: '100%',
	position: 'absolute',
	pointerEvents: 'none',
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
	color: 'inherit',
	'& .MuiInputBase-input': {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)})`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			width: '10rem',
			'&:focus': {
				width: '25rem',
			},
		},
	},
}));

const HeaderSearch: FC<TSearchProps> = ({
	placeholder = DEFAULT_PLACEHOLDER,
	sx,
}) => {
	const SEARCH_PARAM = 'q';
	const dispatch = useAppDispatch();
	const [searchParams, setSearchParams] = useSearchParams();

	const [query, setQuery] = useState(() => {
		return searchParams.get(SEARCH_PARAM) ?? '';
	});

	useEffect(() => {
		if (query) {
			searchParams.set(SEARCH_PARAM, query);
			dispatch(setHeaderSearchQuery(query));
		} else {
			searchParams.delete(SEARCH_PARAM);
			dispatch(setHeaderSearchQuery(''));
		}
		setSearchParams(searchParams);
	}, [query, dispatch, searchParams, setSearchParams]);

	return (
		<BaseSearch sx={sx}>
			<SearchIconWrapper>
				<SearchIcon />
			</SearchIconWrapper>
			<StyledInputBase
				value={query}
				placeholder={placeholder}
				inputProps={{ 'aria-label': 'search' }}
				onChange={(e) => setQuery(e.target.value)}
			/>
		</BaseSearch>
	);
};

export default HeaderSearch;
