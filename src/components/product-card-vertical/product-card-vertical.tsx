import { TProduct } from '../../models/models';
import { FC } from 'react';
import { Link, useLocation } from 'react-router-dom';
import Card from '@mui/material/Card';
import DiscountChip from '../discount-chip/discount-chip';
import FavoriteButton from '../favorite-button/favorite-button';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { Stack } from '@mui/material';
import Price from '../price/price';
import Spacer from '../spacer/spacer';
import Typography from '@mui/material/Typography';
import CardActions from '@mui/material/CardActions';
import QuantityButton from '../quantity-button/quantity-button';
import { useTranslation } from 'react-i18next';

type TProductCardProps = {
	product: TProduct;
};

const ProductCardVertical: FC<TProductCardProps> = ({ product }) => {
	const NAVIGATE_PATH = `/product/${product.id}`;
	const { t } = useTranslation();
	const location = useLocation();
	return (
		<Card sx={{ position: 'relative' }}>
			<DiscountChip
				price={product.price}
				discount={product.discount}
				sx={{
					position: 'absolute',
					top: '-1px',
					left: '-1px',
					m: 1,
				}}
			/>
			<FavoriteButton
				product={product}
				sx={{
					position: 'absolute',
					top: '-1px',
					right: '-1px',
					m: 1,
				}}
			/>
			<Link to={NAVIGATE_PATH} state={{ from: location.pathname }}>
				<CardMedia
					component='img'
					alt={product.name}
					image={product.pictures}
					sx={{ height: '10rem' }}
				/>
			</Link>
			<CardContent sx={{ height: '10rem' }}>
				<Stack direction={'row'} spacing={2}>
					<Price price={product.price} discount={product.discount} />
					<Spacer />
					<Typography variant='subtitle2' fontWeight='light'>
						{`${t('text.inStock')} ${product.stock} ${t('text.pc')}`}
					</Typography>
				</Stack>
				<Typography variant='h6'>{product.name}</Typography>
			</CardContent>
			<CardActions>
				<Spacer />
				<QuantityButton productId={product.id} stock={product.stock} />
			</CardActions>
		</Card>
	);
};

export default ProductCardVertical;
