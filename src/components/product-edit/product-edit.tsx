import ProductCreate from '../product-create/product-create';
import { withQuery } from '../../HOCs/with-query';

export default withQuery(ProductCreate);
