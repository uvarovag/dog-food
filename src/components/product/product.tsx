import { FC } from 'react';
import { Grid, Stack } from '@mui/material';
import Price from '../price/price';
import DiscountChip from '../discount-chip/discount-chip';
import Typography from '@mui/material/Typography';
import FavoriteButton from '../favorite-button/favorite-button';
import QuantityButton from '../quantity-button/quantity-button';
import { TProduct } from '../../models/models';
import { withQuery } from '../../HOCs/with-query';
import Review from '../review/review';
import ReviewCreate from '../review-create/review-create';
import { useTranslation } from 'react-i18next';

type TProductProps = {
	product: TProduct | null;
};

const Product: FC<TProductProps> = ({ product }) => {
	const { t } = useTranslation();
	return (
		<>
			{product && (
				<Grid container spacing={2}>
					<Grid item xs={12} lg={6}>
						<img
							width={'100%'}
							src={product.pictures}
							alt={product.name}
							loading='lazy'
						/>
					</Grid>
					<Grid item xs={12} lg={6}>
						<Stack direction={'column'} spacing={2}>
							<Stack direction={'row'} spacing={2}>
								<Price price={product.price} discount={product.discount} />
								<DiscountChip
									price={product.price}
									discount={product.discount}
								/>
							</Stack>
							<Typography variant='subtitle2' fontWeight='light'>
								{`${t('text.inStock')} ${product.stock} ${t('text.pc')}`}
							</Typography>
							<Typography variant='h4'>{product.name}</Typography>
							<Typography variant='body1'>{product.description}</Typography>
							<Stack direction={'row'} spacing={2}>
								<FavoriteButton product={product} />
								<QuantityButton productId={product.id} stock={product.stock} />
							</Stack>
						</Stack>
					</Grid>
					<Grid item xs={12}>
						<ReviewCreate productId={product.id} />
					</Grid>
					<Grid item xs={12}>
						<Review reviews={product.reviews} />
					</Grid>
				</Grid>
			)}
		</>
	);
};

export default withQuery(Product);
