import '@testing-library/jest-dom';
import { screen } from '@testing-library/react';
import { TEST_ID } from '../../../constants/constants';
import { setupTestWrapper } from '../../../utils/setup-test-wrapper';

describe('Тест для компонента Footer', () => {
	beforeEach(() => {
		setupTestWrapper({ initialRouterPath: '/' });
	});

	test('Проверяем доступность Logo link', () => {
		expect(
			screen.getByTestId(TEST_ID.FOOTER.FOOTER_LOGO_LINK)
		).toBeInTheDocument();
	});
});
