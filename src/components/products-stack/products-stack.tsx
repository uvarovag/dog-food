import { TProduct } from '../../models/models';
import { FC } from 'react';
import { Stack } from '@mui/material';
import {
	ProductCardHorizontalWithSelect,
	TProductCardProps,
} from '../product-card-horizontal/product-card-horizontal';
import { withQuery } from '../../HOCs/with-query';
import { TWithSelectProps } from '../../HOCs/with-select';

type TProductsStackProps = {
	products: TProduct[] | null;
	selected: string[];
	onSelect: TWithSelectProps<TProductCardProps>['onSelect'];
};

const ProductsStack: FC<TProductsStackProps> = ({
	products,
	selected,
	onSelect,
}) => {
	return (
		<>
			{products && (
				<Stack direction={'column'} spacing={2}>
					{products.map((product) => (
						<ProductCardHorizontalWithSelect
							key={product.id}
							product={product}
							selected={selected.includes(product.id)}
							onSelect={onSelect}
							checkBoxSx={{ m: 1 }}
						/>
					))}
				</Stack>
			)}
		</>
	);
};

export default withQuery(ProductsStack);
