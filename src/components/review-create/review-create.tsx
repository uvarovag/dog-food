import { TReview } from '../../models/models';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	SxProps,
	TextField,
} from '@mui/material';
import { FC, useRef, useState } from 'react';
import { useCreateReviewMutation } from '../../store/api/products-api';
import * as yup from 'yup';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import Box from '@mui/material/Box';
import { Rating } from '@mui/lab';
import LoadingButton from '@mui/lab/LoadingButton';
import { useTranslation } from 'react-i18next';

export type TReviewCreateFormValue = Pick<TReview, 'rating' | 'text'>;

type TReviewCreateProps = {
	productId: string;
	sx?: SxProps;
};

const ReviewCreate: FC<TReviewCreateProps> = ({ productId, sx }) => {
	const { t } = useTranslation();
	const ref = useRef<HTMLFormElement>();
	const [open, setOpen] = useState(false);
	const [createReviewMutation] = useCreateReviewMutation();

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const formSchema = yup.object({
		rating: yup.number().required(),
		text: yup.string().required().min(6).max(200),
	});

	const {
		control,
		handleSubmit,
		reset,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TReviewCreateFormValue>({
		defaultValues: {
			rating: 0,
			text: '',
		},
		resolver: yupResolver(formSchema),
	});

	const submitHandler: SubmitHandler<TReviewCreateFormValue> = async (
		values
	) => {
		try {
			await createReviewMutation({
				productId,
				...values,
			});
			toast.success(t('message.changesSaved'));
			reset();
			handleClose();
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<>
			<Button variant='outlined' onClick={handleClickOpen} sx={sx}>
				{t('title.createReview')}
			</Button>
			<Dialog open={open} onClose={handleClose}>
				<DialogTitle>{t('title.createReview')}</DialogTitle>
				<DialogContent>
					<Box
						component='form'
						ref={ref}
						onSubmit={handleSubmit(submitHandler)}
						noValidate>
						<Controller
							name='rating'
							control={control}
							defaultValue={3}
							rules={{ required: true }}
							render={({ field }) => (
								<Rating
									size='large'
									value={+field.value}
									onChange={field.onChange}
								/>
							)}
						/>
						<Controller
							name={'text'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									multiline
									rows={4}
									required
									fullWidth
									label={t('label.text')}
									autoComplete='text'
									error={!!errors.text?.message}
									helperText={errors.text?.message}
									{...field}
								/>
							)}
						/>
					</Box>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose}>{t('button.cancel')}</Button>
					<LoadingButton
						disabled={isSubmitted && (!isValid || isSubmitting)}
						loading={isSubmitting}
						variant='contained'
						onClick={() => ref?.current?.requestSubmit()}>
						{t('button.save')}
					</LoadingButton>
				</DialogActions>
			</Dialog>
		</>
	);
};

export default ReviewCreate;
