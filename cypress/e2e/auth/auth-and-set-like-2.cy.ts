/// <reference types="cypress" />

import {TEST_ID} from "../../../src/constants/constants";

const randEmail = `my-test${Math.floor(Math.random() * 10000)}@mail.ru`
const group = 'sb-09'

describe('Регистрация нового пользователя', () => {
	beforeEach(() => {
		cy.visit('/signup');
	});

	it('Проверяем регистрацию нового пользователя', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="group"]`).type(group);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGOUT_BUTTON}]`).click({force: true});
	});
});

describe('Авторизация и установка лайков', () => {
	beforeEach(() => {
		cy.visit('/signin');
	});
	it('Авторизация и установка лайков', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNIN_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.PAGE.PRODUCTS_PAGE}]`).should('exist');

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(2)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_NOT_ACTIVE}]`)
		.click();

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(2)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_ACTIVE}]`)
		.should('exist');

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(5)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_NOT_ACTIVE}]`)
		.click();

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(5)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_ACTIVE}]`)
		.should('exist');


		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(2)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_ACTIVE}]`)
		.click();

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(2)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_NOT_ACTIVE}]`)
		.should('exist');

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(5)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_ACTIVE}]`)
		.click();

		cy.get(`[data-testid=${TEST_ID.COMPONENT.COMPONENT_RODUCTS_GRID_ITEM}]`)
		.eq(5)
		.find(`[data-testid=${TEST_ID.BUTTON.FAVORITE_NOT_ACTIVE}]`)
		.should('exist');
	});
});
