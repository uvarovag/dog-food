/// <reference types="cypress" />

import {TEST_ID} from "../../../src/constants/constants";

const randEmail = `my-test${Math.floor(Math.random() * 10000)}@mail.ru`;
const group = 'sb-09';
const randProductName = `my product name ${Math.floor(Math.random() * 10000)}`;
const randQuantity = `${Math.floor(Math.random() * 1000)}`;

describe('Регистрация нового пользователя', () => {
	beforeEach(() => {
		cy.visit('/signup');
	});

	it('Проверяем регистрацию нового пользователя', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="group"]`).type(group);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGOUT_BUTTON}]`).click({force: true});
	});
});

describe('Авторизация и создание карточки продукта', () => {
	beforeEach(() => {
		cy.visit('/signin');
	});

	it('Проверяем валидацию формы', () => {
		cy.get(`[name="email"]`).type('not-valid-email');
		cy.get(`[name="password"]`).type('000');
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNIN_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNIN_FORM_BUTTON}]`).should('be.disabled');
		cy.contains('email must be a valid email');
		cy.contains('password must be at least 6 characters');
	});

	it('Авторизация и логаут', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNIN_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.PAGE.PRODUCTS_PAGE}]`).should('exist');
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGOUT_BUTTON}]`).click({force: true});
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGIN_BUTTON}]`).should('exist');
	});
});
