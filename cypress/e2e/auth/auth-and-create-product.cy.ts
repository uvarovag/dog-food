/// <reference types="cypress" />

import {TEST_ID} from "../../../src/constants/constants";

const randEmail = `my-test${Math.floor(Math.random() * 10000)}@mail.ru`;
const group = 'sb-09';
const randProductName = `my product name ${Math.floor(Math.random() * 10000)}`;
const randQuantity = `${Math.floor(Math.random() * 1000)}`;

describe('Регистрация нового пользователя', () => {
	beforeEach(() => {
		cy.visit('/signup');
	});

	it('Проверяем валидацию формы', () => {
		cy.get(`[name="email"]`).type('not-valid-email');
		cy.get(`[name="password"]`).type('000');
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).should('be.disabled');
		cy.contains('email must be a valid email');
		cy.contains('group is a required field');
		cy.contains('password must be at least 6 characters');
	});

	it('Проверяем регистрацию нового пользователя', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="group"]`).type(group);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGOUT_BUTTON}]`).click({force: true});
	});
});

describe('Авторизация и создание карточки продукта', () => {
	beforeEach(() => {
		cy.visit('/signin');
	});

	it('Авторизация и создание карточки продукта', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[type="submit"]`).should('be.enabled').click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.CTEATE_PRODUCT_BUTTON}]`).click();
		cy.get(`[name="name"]`).type(randProductName);
		cy.get(`[name="description"]`).type(randProductName);
		cy.get(`[name="pictures"]`).type('https://www.mirf.ru/wp-content/uploads/2017/10/mult03-1024x576.png');
		cy.get(`[name="price"]`).type(randQuantity);
		cy.get(`[name="discount"]`).type(randQuantity);
		cy.get(`[name="stock"]`).type(randQuantity);
		cy.get(`[data-testid=${TEST_ID.BUTTON.CTEATE_PRODUCT_FORM_BUTTON}]`).click();
		cy.wait(1000);
		cy.get(`[data-testid=${TEST_ID.HEADER.HEADER_LOGO_LINK}]`).click();
		cy.get(`[aria-label="search"]`).type(randProductName);
		cy.get(`[alt="${randProductName}"]`).click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.FAVORITE_NOT_ACTIVE}]`).click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.FAVORITE_ACTIVE}]`).should('exist');
	});
});
