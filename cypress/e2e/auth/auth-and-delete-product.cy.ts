/// <reference types="cypress" />

import {TEST_ID} from "../../../src/constants/constants";

const randEmail = `my-test${Math.floor(Math.random() * 10000)}@mail.ru`;
const group = 'sb-09';
const randProductName = `my product name ${Math.floor(Math.random() * 10000)}`;
const randQuantity = `${Math.floor(Math.random() * 1000)}`;

describe('Регистрация нового пользователя', () => {
	beforeEach(() => {
		cy.visit('/signup');
	});

	it('Проверяем регистрацию нового пользователя', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="group"]`).type(group);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[data-testid=${TEST_ID.BUTTON.SIGNUP_FORM_BUTTON}]`).click();
		cy.get(`[data-testid=${TEST_ID.HEADER.LOGOUT_BUTTON}]`).click({force: true});
	});
});

describe('Авторизация и удаление товара', () => {
	beforeEach(() => {
		cy.visit('/signin');
	});
	it('Авторизация и удаление товара', () => {
		cy.get(`[name="email"]`).type(randEmail);
		cy.get(`[name="password"]`).type(randEmail);
		cy.get(`[type="submit"]`).should('be.enabled').click();
		cy.get(`[data-testid=${TEST_ID.BUTTON.CTEATE_PRODUCT_BUTTON}]`).click();
		cy.get(`[name="name"]`).type(randProductName);
		cy.get(`[name="description"]`).type(randProductName);
		cy.get(`[name="pictures"]`).type('https://www.mirf.ru/wp-content/uploads/2017/10/mult03-1024x576.png');
		cy.get(`[name="price"]`).type(randQuantity);
		cy.get(`[name="discount"]`).type(randQuantity);
		cy.get(`[name="stock"]`).type(randQuantity);
		cy.get(`[data-testid=${TEST_ID.BUTTON.CTEATE_PRODUCT_FORM_BUTTON}]`).click();
		cy.wait(1000);
		cy.get(`[data-testid=${TEST_ID.BUTTON.DELETE_PRODUCT_PRODUCTCARD_BUTTON}]`).click();
		cy.get(`[aria-label="search"]`).type(randProductName);
		cy.get(`[alt="${randProductName}"]`).should('not.exist');
	});
});
